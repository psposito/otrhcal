��    A      $  Y   ,      �     �     �     �     �  :   �     �  5       L  9   d  �  �  �  #
     �  
                  9  9   L  	   �     �     �     �  %   �     �     	          '     3     A     O     \     k     t     �  +   �     �     �     �                    1     H     Y     t      �     �     �     �  $   �  "     "   >     a     }     �     �     �     �     �               2     8     >     D  �  K     �               !  ;   *     f  .  �     �  >   �  �  	  �  �     �     �     �     �     �  G   �          &     <     Y  #   m     �     �     �     �     �          %     >     Y     _  	   r  I   |     �      �               5     >      [     |  "   �     �  )   �     �          8  #   L  "   p  $   �     �     �     �     �          +  
   G     R     f     w     �     �     �     0      4   =   +          $   #                  	   7            ?   (   
         >      2   8       &                  3          -   *      )   %      "      5                <                                                         ;              ,   !   A         6              '   /       :   9       1   @   .        days after Pascha  days before Pascha  is:  was: <b>Double click</b> to change from one source to another.
 <b>Lazarus</b> Saturday <b>OrthCal, the Eastern Orthodox Calendar</b>, v. 1.4
		        <a href="http://launchpad.net/orthcal">Homepage</a> - - <a href="https://www.transifex.com/projects/p/OrthCal">Translations</a>
         (C) 2012 <a href="mailto:dgkontopoulos@member.fsf.org?Subject=OrthCal">Dimitrios - Georgios Kontopoulos</a>
 <b>Zacchaeus</b> Sunday <i>Please restart OrthCal for changes to take effect.</i> <span size="small"><b><u>License:</u></b>
<i>This program is free software; you can redistribute it and/or modify it
under the terms of the <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License, as published by the
Free Software Foundation; either version 3 of the License, or (at your
option) any later version</a>.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</i>

Images and content mentioned at the Content Sources section are
offered under their original respective licenses.</span>
 <span size="small">This application was written for the <a href="http://developer.ubuntu.com/showdown/">Ubuntu App Showdown contest</a> of
July 2012, mainly as practice.

The indicator's code is written in <a href="http://www.python.org/">Python</a>/<a href="http://www.pygtk.org/">PyGTK</a>, whereas the main part
of the calendar is written in <a href="http://www.perl.org/">Perl</a>/<a href="http://search.cpan.org/~xaoc/Gtk2-1.244/lib/Gtk2.pm">GTK2</a>.</span>
 About OrthCal About Perl About gtk2-perl All Saints of North America Ascension Thursday Cheesefare (Forgiveness) Sunday / Expulsion from Paradise Completed Content sources Daily Feasts source Day of the Holy Spirit Fathers of the 1st Ecumenical Council Feast search -- OrthCal Great Lent begins Holy Friday Holy Monday Holy Saturday Holy Thursday Holy Tuesday Holy Wednesday Language Leavetaking of Pascha License Meat Fare Sunday (Sunday of Last Judgement) Midfeast of Pentecost No feast could be found. Options for OrthCal Palm Sunday Pascha Pascha calculator -- OrthCal Pascha date calculator Pentecost Sunday Publican & Pharisee Sunday Search for feasts by name Start OrthCal on system startup. Sunday of All Saints Sunday of Myrrh-bearing Women Sunday of Orthodoxy Sunday of St. <b>Gregory</b> Palamas Sunday of St. <b>John</b> Climacus Sunday of St. <b>Mary</b> of Egypt Sunday of St. <b>Thomas</b> Sunday of the Blind Man Sunday of the Holy Cross Sunday of the Paralytic Sunday of the Prodigal Son Sunday of the Samaritan Woman Thanks The date of Pascha in  Translate OrthCal... _Edit _File _Help _Tools Project-Id-Version: OrthCal
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-10-21 18:26+0300
PO-Revision-Date: 2012-10-21 15:28+0000
Last-Translator: dgkontopoulos <dgkontopoulos@member.fsf.org>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ro_RO
Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));
 zile după Paști zile înainte de Paști  este:  a fost: <b>Dublu clic</b> pentru a schimba de la o sursă la alta.
 Duminica lui <b>Lazăr</b> <b>OrthCal, Calendarul Bisericii Ortodoxe</b>, v. 1.4
		    <a href="http://launchpad.net/orthcal">Homepage</a> - - <a href="https://www.transifex.com/projects/p/OrthCal">Translations</a>
      (C) 2012 <a href="mailto:dgkontopoulos@member.fsf.org?Subject=OrthCal">Dimitrios - Georgios Kontopoulos</a>
 Duminica lui <b>Zaheu</b> <i>Repornește OrthCal pentru ca efectele să aibă efect.</i> <span size="small"><b><u>Licenta:</u></b>
<i>Acest program este software liber; îl poți redistribui și/ori modifica
în termenii <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License, cum a fost publicată în Free
Software Foundation; ori versiunea 3 a Licenței, ori (la alegerea
ta) oricare versiune ulterioară</a>.

Acest program este distribuit în speranța că va fi util, dar FĂRĂ
NICI O GARANȚIE; chiar fără garanția implicită de VANDABILITATE
ori POTRIVIRE PENTRU UN ANUMIT SCOP.</i>

Imaginile și conținutul menționate în secțiunea Sursele conținutului
sunt oferite sub licențele lor originale.</span>
 <span size="small">Această aplicație a fost scrisă pentru <a href="http://developer.ubuntu.com/showdown/">Ubuntu App Showdown contest</a> din
iulie 2012, mai mult pentru practică.

Codul indicatorului este scris în <a href="http://www.python.org/">Python</a>/<a href="http://www.pygtk.org/">PyGTK</a>, în timp ce partea cea mai
mare a calendarului este scrisă în <a href="http://www.perl.org/">Perl</a>/<a href="http://search.cpan.org/~xaoc/Gtk2-1.244/lib/Gtk2.pm">GTK2</a>.</span>
 Despre OrthCal Despre Perl Despre gtk2-perl Toți Sfinții Români Joia Înălțării Duminica Lăsatului sec de brânză (a Iertării) / a Izgonirii din Rai Complet Sursele conținutului Sursa Sărbătorilor zilnice Ziua Sfântului Duh Părinții de la Sinodul 1 Ecumenic Căutare sărbători -- OrthCal Începe Postul Mare Sfânta și Marea Joi Sfânta și Marea Luni Sfânta și Marea Sâmbătă Sfânta și Marea Joi Sfânta și Marea Marți Sfânta și Marea Miercuri Limba Odovania Paștelui Licență Duminica Lăsatului sec de carne (Duminica Înfricoșătoarei Judecăți) Înjumătățirea Cincizecimii Nu am găsit nicio sărbătoare. Opțiuni pentru OrthCal Duminica Stâlpărilor Paștele Calculator Paște -- OrthCal Calculator pentru data Paștelui Duminica Cincizecimii Duminica Vameșului și Fariseului Caută sarbătorile după nume Pornește OrthCal la pornirea sistemului. Duminica Tuturor Sfinților Duminica Femeilor Mironosițe Duminica Ortodoxiei Duminica Sf. <b>Grigorie</b> Palama Duminica Sf. <b>Ioan</b> Scărarul Duminica Sf. <b>Maria</b> Egipteanca Duminica Sf. <b>Toma</b>  Duminica Orbului Duminica Sfintei Cruci Duminica Paraliticului Duminica Fiului Risipitor Duminica Femeii Samaritence Mulțumiri Data Paștelui în  Tradu OrthCal... _Editare _Fișier _Ajutor _Unelte 