��    "      ,  /   <      �     �  5       G  �  _     0     >     Z  	   m     w  %   �     �     �     �     �     �     �                    5     I     U     \     m      �     �     �     �     �               7     R  x  p     �	  8  
     <  �  U     N  !   `     �  
   �     �  "   �     �     �                     -     :     K     R     i          �     �  "   �  +   �     �          2     O     h     �     �     �                                              !                                         	       
             "                                                   <b>Lazarus</b> Saturday <b>OrthCal, the Eastern Orthodox Calendar</b>, v. 1.4
		        <a href="http://launchpad.net/orthcal">Homepage</a> - - <a href="https://www.transifex.com/projects/p/OrthCal">Translations</a>
         (C) 2012 <a href="mailto:dgkontopoulos@member.fsf.org?Subject=OrthCal">Dimitrios - Georgios Kontopoulos</a>
 <b>Zacchaeus</b> Sunday <span size="small">This application was written for the <a href="http://developer.ubuntu.com/showdown/">Ubuntu App Showdown contest</a> of
July 2012, mainly as practice.

The indicator's code is written in <a href="http://www.python.org/">Python</a>/<a href="http://www.pygtk.org/">PyGTK</a>, whereas the main part
of the calendar is written in <a href="http://www.perl.org/">Perl</a>/<a href="http://search.cpan.org/~xaoc/Gtk2-1.244/lib/Gtk2.pm">GTK2</a>.</span>
 About OrthCal All Saints of North America Ascension Thursday Completed Day of the Holy Spirit Fathers of the 1st Ecumenical Council Great Lent begins Holy Friday Holy Monday Holy Saturday Holy Thursday Holy Tuesday Holy Wednesday Language Leavetaking of Pascha Options for OrthCal Palm Sunday Pascha Pentecost Sunday Publican & Pharisee Sunday Start OrthCal on system startup. Sunday of All Saints Sunday of Orthodoxy Sunday of St. <b>Thomas</b> Sunday of the Blind Man Sunday of the Holy Cross Sunday of the Paralytic Sunday of the Prodigal Son Sunday of the Samaritan Woman Project-Id-Version: OrthCal
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-10-21 18:26+0300
PO-Revision-Date: 2012-10-21 15:29+0000
Last-Translator: dgkontopoulos <dgkontopoulos@member.fsf.org>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=2; plural=(n != 1);
 Sábado de <b>Lázaro</b> <b>OrthCal, el calendario ortodoxo del este</b>, v. 1.4
		        <a href="http://launchpad.net/orthcal">Sitio web</a> - - <a href="https://www.transifex.com/projects/p/OrthCal">Traducciones</a>
         (C) 2012 <a href="mailto:dgkontopoulos@member.fsf.org?Subject=OrthCal">Dimitrios - Georgios Kontopoulos</a>
 Domingo de <b>Zaqueo</b> <span size="small">Esta aplicación se escribió para el <a href="http://developer.ubuntu.com/showdown/">concurso Ubuntu App Showdown</a> de
julio de 2012, principalmente como una práctiva.

El código del indicador está escrito en <a href="http://www.python.org/">Python</a>/<a href="http://www.pygtk.org/">PyGTK</a>, mientras que la
parte principal del calendario está escrita en <a href="http://www.perl.org/">Perl</a>/<a href="http://search.cpan.org/~xaoc/Gtk2-1.244/lib/Gtk2.pm">GTK2</a>.</span>
 Acerca de OrthCal Todos los santos de Norteamérica Jueves de Ascensión Completado Día del Espíritu Santo Padres del 1er Concilio Ecuménico Comienza la gran cuaresma Viernes santo Lunes santo Sábado santo Jueves santo Martes santo Miércoles santo Idioma Despedida de la Pascua Opciones para OrthCal Domingo de ramos Pascua Domingo de Pentecostés Domingo del publicano y el fariseo Iniciar OrthCal cuando arranque el sistema. Domingo de todos los santos Domingo de la ortodoxia Domingo de San <b>Tomás</b> Domingo del hombre ciego Domingo de la Santa Cruz Domingo del paralítico Domingo del hijo pródigo Domingo de la mujer samaritana 